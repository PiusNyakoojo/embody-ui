
interface User {
  firstName: string
  lastName: string
  email: string
  phone?: string
}

interface ProfileState {
    user?: User
    error: boolean
}

export {
  User,
  ProfileState
}
