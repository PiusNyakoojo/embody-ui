import axios from 'axios'
import { Buffer } from 'buffer'

const API_ROOT = 'localhost:8181'
const TEST_API = 'https://i.imgur.com/CgMu3RU.jpg'

function startRecordingScreen (callback: (image: any) => void) {
  const ws = new WebSocket(`ws://${API_ROOT}`)

  ws.onopen = (event) => {
    console.log('Opened connection!')

    const message = {
      type: 'start-record-screen'
    }

    ws.send(JSON.stringify(message))
  }

  ws.onmessage = (message) => {
    callback(message)
    // console.log('Received message: ', message)
  }
}

function moveMousePosition (dx: number, dy: number): void {
  axios.post(`http://${API_ROOT}/v0/robot/actuator/mouse`, {
    dx,
    dy
  }).then((response) => {
    // console.log('Response: ', response.data.message)
  })
}

async function getScreenImage (): Promise<string|ArrayBuffer> {
  return Promise.resolve('')
}

// async function getCameraImage (): Promise<string|ArrayBuffer> {
//   // const data: any = await axios.get(`${API_ROOT}/v0/robot/sensor/camera`)
//   const data: any = await axios.get(`${TEST_API}`)
//   const reader: FileReader = new FileReader()
//
//   // console.log('Data: ', data)
//   const dataBlob = new Blob([data.data], { type: data.headers['content-type'] })
//
//   return new Promise((resolve, reject) => {
//     reader.onloadend = (): any => {
//       // console.log('Ended', reader.result)
//       resolve(reader.result || '')
//     }
//     reader.readAsDataURL(dataBlob)
//   })
//
//   // const bitmap: any = data.data.image
//   // const bufferParse: any = new Buffer(bitmap.image.data, 'base64').toString('base64')
//
//   // return Promise.resolve(data.data)
// }


export default {
  startRecordingScreen,
  moveMousePosition,
  getScreenImage
}
