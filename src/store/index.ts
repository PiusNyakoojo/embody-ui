import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

import { RootState } from './store-types'

import robot from './robot'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
  state: { appName: '' },
  modules: {
    robot
  }
}

export default new Vuex.Store<RootState>(store)
