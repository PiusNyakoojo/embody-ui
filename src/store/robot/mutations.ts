import { MutationTree } from 'vuex'
import { RobotState } from './types'

const SET_ROBOT_NAME = 'SET_ROBOT_NAME'

const mutations: MutationTree<RobotState> = {
  [SET_ROBOT_NAME] (state: RobotState, name: string) {
    'state.name = name'
  }
}

export {
  mutations,
  SET_ROBOT_NAME
}
