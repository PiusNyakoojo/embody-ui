
interface Robot {
  uid: string
  rid: string

  alias: string
  description: string
  iconURL: string

  sensors: { [id: string]: any }
  actuators: { [id: string]: any }

  aiSystem: RobotAI
}

interface RobotAI {
  agentID: string
  mindID: string
  commands: { [id: string]: any }
}

interface RobotState {
  robotMap?: { [uid: string]: Robot }
}

export {
  RobotState
}
