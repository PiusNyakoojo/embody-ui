import { ActionTree, Commit } from 'vuex'

import { RobotState } from './types'
import { RootState } from '../store-types'

import service from '../../service-manager'

import { SET_ROBOT_NAME } from './mutations'

interface ActionParam {
  commit: Commit
}

const actions: ActionTree<RobotState, RootState> = {
  setRobotName ({ commit }: ActionParam, title: string) {
    commit(SET_ROBOT_NAME, title)
  },

  async getScreenImage ({ }: ActionParam): Promise<string|ArrayBuffer> {
    return service.getScreenImage()
  },

  async startRecordingScreen ({ }: ActionParam, callback: (image: any) => void) {
    return service.startRecordingScreen(callback)
  },

  moveMousePosition ({ }: ActionParam, { dx, dy }: { dx: number, dy: number }) {
    service.moveMousePosition(dx, dy)
  }
}

export default actions
