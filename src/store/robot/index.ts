import { Module } from 'vuex'

import { RootState } from '../store-types'

import getters from './getters'
import actions from './actions'
import { mutations } from './mutations'
import { RobotState } from './types'

const state: RobotState = {
  robotMap: {}
}

const namespaced: boolean = true

const robot: Module<RobotState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
}

export default robot
